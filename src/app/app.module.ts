import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './User/header/header.component';
import { HomeComponent } from './User/home/home.component';
import { ProjectComponent } from './User/project/project.component';
import { ServicesComponent } from './User/services/services.component';
import { FooterComponent } from './footer/footer.component';
import { ChooseComponent } from './User/choose/choose.component';
import { AboutComponent } from './User/about/about.component';
import { ContactComponent } from './User/contact/contact.component';
import { TenderingComponent } from './User/services/tendering/tendering.component';
import { DrawingComponent } from './User/services/drawing/drawing.component';
import { ValueComponent } from './User/services/value/value.component';
import { FacadeComponent } from './User/services/facade/facade.component';
import { FailureComponent } from './User/services/failure/failure.component';
import { GreenComponent } from './User/services/green/green.component';
import { AboutusComponent } from './User/about/aboutus/aboutus.component';
import { ProjectlistComponent } from './User/projectlist/projectlist.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ProjectComponent,
    ServicesComponent,
    FooterComponent,
    ChooseComponent,
    AboutComponent,
    ContactComponent,
    TenderingComponent,
    DrawingComponent,
    ValueComponent,
    FacadeComponent,
    FailureComponent,
    GreenComponent,
    AboutusComponent,
    ProjectlistComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
