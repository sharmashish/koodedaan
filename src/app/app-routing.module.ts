import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './User/header/header.component';
import { HomeComponent } from './User/home/home.component';
import { ServicesComponent } from './User/services/services.component';
import { FooterComponent } from './footer/footer.component';
import { ChooseComponent } from './User/choose/choose.component';
import { AboutComponent } from './User/about/about.component';
import { ContactComponent } from './User/contact/contact.component';
import { TenderingComponent } from './User/services/tendering/tendering.component';
import { DrawingComponent } from './User/services/drawing/drawing.component';
import { ValueComponent } from './User/services/value/value.component';
import { FacadeComponent } from './User/services/facade/facade.component';
import { FailureComponent } from './User/services/failure/failure.component';
import { GreenComponent } from './User/services/green/green.component';
import { AboutusComponent } from './User/about/aboutus/aboutus.component';
import { ProjectlistComponent } from './User/projectlist/projectlist.component';

const routes: Routes = [
  { path: 'header', component: HeaderComponent },
  { path: '', component: HomeComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'choose', component: ChooseComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'tendering', component: TenderingComponent },
  { path: 'drawing', component: DrawingComponent },
  { path: 'value', component: ValueComponent },
  { path: 'facade', component: FacadeComponent },
  { path: 'failure', component: FailureComponent },
  { path: 'green', component: GreenComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'projectlist', component: ProjectlistComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
